//Headers
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "KS0108.h"

#include "header/dce.h"
#include "Image.h"
#include <avr/io.h>
#include "Font5x8.h"
const char txt1[] PROGMEM = "Big";
const char txt2[] PROGMEM = "Small";

#define UP (PINB&(1<<PINB0))
#define DN (PINB&(1<<PINB1))
void font() {
	uint8_t w = 32, h = 40;
	GLCD_SetFont(Frankfurter32x40, w, h, GLCD_Merge);
}
void Gotoxy() {
	GLCD_GotoXY(51, 6);
}
int main(void) {
	DDRB = (0 << PINB0) | (0 << PINB1);
	PORTB = (1 << PINB0) | (1 << PINB1);

	//Setup
	GLCD_Setup();
	GLCD_Clear();
//
	////Inverted mode
	////GLCD_InvertMode();
	//
	////Print text #1
	//GLCD_SetFont(Tekton_Pro_Ext27x28, 27, 28, GLCD_Merge);
	//GLCD_GotoXY(42, 1);
	//GLCD_PrintString_P(txt1);
//
	////Print text #2
	////GLCD_SetFont(Tahoma11x13, 11, 13, GLCD_Merge);
	//GLCD_GotoXY(48, 30);
	//GLCD_PrintString_P(txt2);
	//
	////Draw outline
	//GLCD_DrawRoundRectangle(1, 1, 126, 62, 5, GLCD_Black);
	//GLCD_Render();
	////Render screen
	////GLCD_Render();
	//_delay_ms(200);
	//GLCD_Clear();
	////Loop
	//GLCD_GotoXY(20,25);
	//GLCD_PrintString_P(txt3);
	//GLCD_Render();
	//_delay_ms(200);
	//GLCD_Clear();
	//GLCD_GotoXY(20,40);
	////GLCD_SetFont(Tahoma11x13, 11, 13, GLCD_Merge);
	//GLCD_FillScreen(GLCD_Black);
	//GLCD_DrawCircle(50,30,30,GLCD_White);
	//GLCD_Clear();
	//_delay_ms(200);
	//GLCD_SetFont(Tekton_Pro_Ext27x28, 27, 28, GLCD_Merge);

	uint8_t x = 0, i = 0, id = 6;
	//GLCD_InvertMode();
	while (1) {

		//GLCD_SetFont(abc, 21, 32, GLCD_Merge);
		//GLCD_SetFont(Tekton_Pro_Ext27x28, 27, 28, GLCD_Merge);
		//GLCD_GotoXY(50,25);
		//GLCD_PrintChar('0');
		//GLCD_Render();

		//GLCD_PrintString("123 TCL");

		//GLCD_GotoXY(0,0);
		//GLCD_DrawBitmap(img,128,64,0xff);
		//GLCD_Render();
		//GLCD_Clear();
		//_delay_ms(200);

		/*for (i; i < 80; i+=5) {

			GLCD_FillRoundRectangle(i-5, 37, i + 1, 37 + 25, 0, GLCD_White);
			GLCD_Render();
			GLCD_GotoXY(i, 37);
			GLCD_DrawBitmap(user25px, 25, 25, 0xff);
			GLCD_Render();

		}*/

		GLCD_SetFont(Font5x8,5,8,GLCD_Merge);
		GLCD_GotoXY(10, 45);
		//GLCD_PrintString("012345678");
		GLCD_PrintChar('A');
		GLCD_Render();
		//GLCD_DrawRoundRectangle(1, 1, 126, 62, 5, GLCD_Black);
		GLCD_Render();

		if (UP == 0) {
			//GLCD_Clear();
			x++;
			while (UP == 0) {
			};
		}
		if (DN == 0) {
			//GLCD_Clear();
			x--;
			while (DN == 0)
				;
		}

		switch (x) {
		case 0: {

			GLCD_FillRoundRectangle(48, 8, 72, 42, 5, GLCD_White);
			GLCD_GotoXY(105, 0);
			GLCD_DrawBitmap(batterylevel0_15x40, 15, 40, 0xff);
			font();
			Gotoxy();

			//GLCD_PrintChar('0');
			GLCD_PrintInteger(x);
			GLCD_Render();
			break;
		}
		case 1: {
			GLCD_FillRoundRectangle(48, 8, 72, 42, 5, GLCD_White);
			GLCD_GotoXY(105, 0);
			GLCD_DrawBitmap(batterylevel1_15x40, 15, 40, 0xff);
			font();
			Gotoxy();

			GLCD_PrintInteger(x);
			GLCD_Render();
			break;
		}
		case 2: {
			GLCD_FillRoundRectangle(48, 8, 72, 42, 5, GLCD_White);
			GLCD_GotoXY(105, 0);
			GLCD_DrawBitmap(batterylevel2_15x40, 15, 40, 0xff);
			font();
			Gotoxy();
			GLCD_PrintInteger(x);
			GLCD_Render();
			break;
		}
		case 3: {
			GLCD_FillRoundRectangle(48, 8, 72, 42, 5, GLCD_White);
			GLCD_GotoXY(105, 0);
			GLCD_DrawBitmap(batterylevel3_15x40, 15, 40, 0xff);
			font();
			Gotoxy();
			GLCD_PrintInteger(x);
			GLCD_Render();
			break;
		}
		case 4: {
			GLCD_FillRoundRectangle(48, 8, 72, 42, 5, GLCD_White);
			GLCD_GotoXY(105, 0);
			GLCD_DrawBitmap(batterylevel4_15x40, 15, 40, 0xff);
			font();
			Gotoxy();
			GLCD_PrintInteger(x);
			GLCD_Render();
			break;
		}
		}
	}
}
